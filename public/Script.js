function updatePrice() {
  // ������� select �� ����� � DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let a = document.getElementsByName("Amount");
  let amount = a[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  
  // �������� ��� ���������� �����������.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "1" || select.value == "3" ? "none" : "block");
  
  // ������� ����� �������� ����� �������.
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        if (select.value=="1"|| select.value=="3"){
          optionPrice = 0;
        }
        price += optionPrice;
      }
    }
  });

  // �������� ��� ���������� ��������.
  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = (select.value == "1" || select.value == "2" ? "none" : "block");

  // ������� ����� �������� �������� �������.
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        if (select.value=="1"|| select.value=="2"){
          propPrice = 0;
        }
        price += propPrice;
      }
    }
  });
  
  let prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = amount.value*price + "$";
}

function getPrices() {
  return {
    prodTypes: [1000, 1100, 1500, 2000, 2000000],
    prodOptions: {
      option1: 1000,
      option2: 2000,
      option3: 5000,
    },
    prodProperties: {
      prop1: 1999,
      prop2: 2999,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
  // �������� �����������.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  
  // ������� select �� ����� � DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  // ��������� ���������� �� ��������� select.
  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });

  // ������� amount �� ����� � DOM.
  let a = document.getElementsByName("Amount");
  let amount = a[0];
  // ��������� ���������� �� ��������� amount.
  amount.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    updatePrice();
  });

  // ��������� ���������� �����������.  
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });

    // ��������� ���������� �����������.  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });

  updatePrice();
});

